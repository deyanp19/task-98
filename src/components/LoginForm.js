import styles from "./LoginForm.module.css";
 
function LoginForm() {
    
const handlerSubmit=()=>{};

    return (
        <div>
            <form onSubmit={handlerSubmit}>
                <label className={styles.label}  htmlFor="username">User name</label>
                <input className={styles.input} id="username" name="username" type="text" />
                <label className={styles.label}  htmlFor="password">Password</label>
                <input className={styles.input} id="password" name="password" type="text" />
                <button className={styles.submit} type="submit">Submit</button>
            </form>
        </div>


    )
}


export default LoginForm;
